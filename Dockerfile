FROM elixir:1.3.3

ENV NODE_VERSION 10.x
ENV NPM_VERSION 6.2.0
# ENV MIX_ENV=dev
# ENV MIX_ENV=prod

RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash \
  && apt update \
  && apt -y install nodejs \
  && apt -y install inotify-tools \
  && npm install \
  && apt-get clean

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new-1.2.5.ez --force

# CMD mix phoenix.server
# CMD elixir --detached -S mix do compile, phoenix.server

WORKDIR /app
